package com.supermap.gaf.common.storage.enums;

/**
 * @author heykb
 */
public enum ConfigSelectMode {
    GLOBAL, TENANT, TENANT_FIRST;
}